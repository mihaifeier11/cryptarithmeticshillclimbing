import random
import time
from helper import *

def hillClimbing(number, equation, dictionary, firstElementsIndexList, numberOfDistantNeighbours):
    """
    the Hill Climbing algorithm for the Cryptarithmetic problem
    :param number:                      the number of lines to be added
    :param equation:                    a list of list containing every line of the equation
    :param dictionary:                  the dictionary with the letter as the key and the index for the list as the value
    :param firstElementsIndexList:      the set containing the indexes for the first letter from every word
    :param numberOfDistantNeighbours:   the number of distant neighbours to be generated
    :return:
        @list           - the result
        @fitnessList    - a list containing the fitness of the solution from every step
    """
    fitnessList = []
    previous = []
    count = 0
    list = generateRandomList(firstElementsIndexList)
    result = solver(number, equation, dictionary, list)

    while result != 0:
        previous.append(list)
        neighbours = generateNeighbours(numberOfDistantNeighbours, list, firstElementsIndexList)
        list = best(number, equation, dictionary, neighbours)

        while list in previous:
            list = getRandomNeighbour(neighbours)

        if count > 100:
            count = 0
            previous = []

        fitnessList.append(result)
        result = solver(number, equation, dictionary, list)
        count += 1
        print(solver(number, equation, dictionary, list))

    return list, fitnessList