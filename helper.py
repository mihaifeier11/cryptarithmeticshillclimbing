import random



def isValid(list, firstElementsIndexSet):
    """
    checks if the list has the number 0 as a value for any index in the set given
    :param list:                    the list containing a permutation
    :param firstElementsIndexSet:   the set containing the indexes for the first letter from every word
    :return:
        True    - if the condition is met
        False   - otherwise
    """
    for index in firstElementsIndexSet:
        if list[index] == 0:
            return False

    return True


def generateRandomList(firstElementsIndexSet):
    """
    generates a random list of size 10 with unique numbers in the range 0-10 and checks whether the generated list is valid
    :param firstElementsIndexSet: the set containing the indexes for the first letter from every word
    :return:
        @list - the generated list
    """
    list = [0 for i in range(10)]

    while not isValid(list, firstElementsIndexSet):
        list = [0 for i in range(10)]
        count = 1

        while count < 10:
            r = random.randrange(10)
            if list[r] == 0:
                list[r] = count
                count += 1

    return list


def generateCloseNeighbours(list, firstElementsIndexSet):
    """
    generates the close, valid neighbours of a list
        * a neighbour is distant if the exactly one permutation away from the original list
    :param list:                    the list from which the neighbours are generated
    :param firstElementsIndexSet:   the set containing the indexes for the first letter from every word
    :return:
        @neighbours - the list of all the close, valid neighbours from the given list
    """
    neighbours = []

    for i in range(10):
        for j in range(i + 1, 10):
            tempList = list[:]
            tempList[i], tempList[j] = tempList[j], tempList[i]
            if isValid(tempList, firstElementsIndexSet):
                neighbours.append(tempList)

    return neighbours


def generateDistantNeighbours(number, list, firstElementsIndexSet):
    """
    generates a number of distant neighbours of a given list
        * a neighbour is distant if the its more than one permutation away from the original list
    :param number:                  the number of distant neighbours to be generated
    :param list:                    the list from which the neighbours are generated
    :param firstElementsIndexSet:   the set containing the indexes for the first letter from every word
    :return:
        @neighbours - the list of all the distant, valid neighbours from the given list
    """
    neighbours = []
    number = random.randrange(number)
    for i in range(number):
        nrOfPermutations = random.randrange(10)
        copyList = list[:]
        for j in range(nrOfPermutations):
            index1 = random.randrange(10)
            index2 = random.randrange(10)
            if index1 != index2:
                copyList[index1], copyList[index2] = copyList[index2], copyList[index1]

        if isValid(copyList, firstElementsIndexSet):
            neighbours.append(copyList)

    return neighbours


def generateNeighbours(numberOfDistantNeighbours, list, firstElementsIndexList):
    """
    generates the close and distant neighbours
    :param numberOfDistantNeighbours:   the number of distant neighbours to be generated
    :param list:                        the list from which the neighbours are generated
    :param firstElementsIndexList:      the set containing the indexes for the first letter from every word
    :return:
        @neighbours - the list containing both the close and the distant neighbours
    """
    neighbours = []
    closeNeighbours = generateCloseNeighbours(list, firstElementsIndexList)
    distantNeighbours = generateDistantNeighbours(numberOfDistantNeighbours, list, firstElementsIndexList)

    neighbours.extend(closeNeighbours)
    neighbours.extend(distantNeighbours)

    return neighbours


def best(number, equation, dictionary, list):
    """
    finds the best solution from the given list
    :param number:      the number of lines to be added
    :param equation:    a list of list containing every line of the equation
    :param dictionary:  the dictionary with the letter as the key and the index for the list as the value
    :param list:        the list given from which the best is found
    :return:
        @best - the best solution
    """
    best = list[0]
    bestFitness = solver(number, equation, dictionary, best)
    for neighbour in list:
        neighbourFitness = solver(number, equation, dictionary, neighbour)
        if bestFitness > neighbourFitness:
            best = neighbour
            bestFitness = neighbourFitness

    return best


def solver(number, equation, dictionary, list):
    """
    solves the equation with the given values
    :param number:      the number of lines to be added
    :param equation:    a list of list containing every line of the equation
    :param dictionary:  the dictionary with the letter as the key and the index for the list as the value
    :param list:        the list given from which the values are taken
    :return:
        @result - the absolute value of the result from the difference between the expected result and the actual result
    """
    actualResult = 0
    expectedResult = 0

    for line in equation:
        value = 0
        for element in line:
            value = value * 10 + list[dictionary[element]]

        if number > 0:
            actualResult += value
            number -= 1
        else:
            expectedResult = value

    return abs(actualResult - expectedResult)


def getRandomNeighbour(neighbours):
    """
    finds a random neighbour
    :param neighbours: the list of neighbours in which the random neighbour is taken from
    :return:
        @neighbour - a random neighbour
    """
    index = random.randrange(len(neighbours))

    return neighbours[index]
