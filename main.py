import sys
import time

import matplotlib.pyplot as plt

from algorithm import hillClimbing
from utils import *

inputFile = sys.argv[1]
outputFile = sys.argv[2]

number, equation, firstElementsLetterList = readInput(inputFile)
dictionary, firstElementsIndexList = createDictionary(equation, firstElementsLetterList)

if len(sys.argv) == 3:
    numberOfDistantNeighbours = 100
else:
    numberOfDistantNeighbours = int(sys.argv[3])

startTime = time.time()
result, fitnessList = hillClimbing(number, equation, dictionary, firstElementsIndexList, 50)
endTime = time.time()

printSolution(dictionary, result)
printToFile(outputFile, dictionary, result)

print("Operation took {}s.".format(round(endTime - startTime, 4)))

plt.plot(result)
plt.show()
