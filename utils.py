def readInput(file):
    """
    reads the input from the file and returns the values
    :param file: the file from where the input is read
    :return:
        @number                     - the number of lines to be added
        @equation                   - a list of list containing every line of the equation
        @firstElementsLetterList    - the list containing the first letters from every word
    """
    f = open(file, "r")
    equation = []
    number = int(f.readline())
    firstElementsLetterList = []

    for i in range(number + 1):
        line = f.readline()
        line = line.replace("\n", "")
        line = line.split(",")
        firstElementsLetterList.append(line[0])

        equation.append(line)

    f.close()

    return number, equation, firstElementsLetterList


def createDictionary(equation, firstElementsLetterList):
    """
    creates a dictionary and a set of indexes for the first letters in every word of the equation
    :param equation:                    a list of list containing every line of the equation
    :param firstElementsLetterList:     a list containing the first letters from every word
    :return:
        @dictionary             - the dictionary with the letter as the key and the index for the list as the value
        @firstElementsIndexSet  - a set containing the indexes for the first letters of every word
    """
    dictionary = {}
    firstElementsIndexSet = set()
    count = 0

    for element in equation:
        for letter in element:
            if letter not in dictionary.keys():
                dictionary[letter] = count
                count += 1

    for element in equation:
        for letter in element:
            if letter not in dictionary.keys():
                dictionary[letter] = count
                count += 1

    for i in range(count, 10):
        dictionary[i] = i

    for letter in firstElementsLetterList:
        firstElementsIndexSet.add(dictionary[letter])

    return dictionary, firstElementsIndexSet


def printSolution(dictionary, list):
    """
    prints the solution to the console
    :param dictionary:  the dictionary with the letter as the key and the index for the list as the value
    :param list:        the list given from which the values are taken
    :return: -
    """
    for element in dictionary:
        if isinstance(element, str):
            print(element + ": " + str(list[dictionary[element]]))

def printToFile(filename, dictionary, list):
    """
    prints the solution to the given file
    :param filename:    the name of the file
    :param dictionary:  the dictionary with the letter as the key and the index for the list as the value
    :param list:        the list given from which the values are taken
    :return: -
    """
    file = open(filename, "w")

    for element in dictionary:
        if isinstance(element, str):
            file.write(element + ", " + str(list[dictionary[element]]) + "\n")

    file.close()
